﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FayveBot.Utils.Currency
{
  public class CurrencyUser
  {
    private ulong _id { get; set; }

    public ulong Id { get { return _id; } set { _id = value; } }
    public int Currency { get; set; }

    public CurrencyUser() { }

    public CurrencyUser(ulong id) => _id = id;
  }
}
