﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace FayveBot.Utils.LogClasses
{
  public class LogService
  {
    private readonly DiscordSocketClient _client;
    private readonly CommandService _commands;

    private static bool _isInitialised { get; set; } = false;

    public LogService(DiscordSocketClient client, CommandService commands)
    {
      _client = client;
      _commands = commands;

      if(!_isInitialised)
      {
        _client.Log += ConsoleWrite;
        _commands.Log += ConsoleWrite;
        ConsoleWrite(new LogMessage(LogSeverity.Info, "Service", "Logging service initialised"));
        _isInitialised = true;
      }
    }

    public Task ConsoleWrite(LogSeverity severity, string source, string message)
    {
      ConsoleWrite(new LogMessage(severity, source, message));
      return Task.CompletedTask;
    }

    public Task ConsoleWrite(LogSeverity severity, object source, string message)
    {
      ConsoleWrite(new LogMessage(severity, source.GetType().Name, message));
      return Task.CompletedTask;
    }

    public Task ConsoleWrite(LogMessage arg)
    {
      switch(arg.Severity)
      {
        case LogSeverity.Critical:
          Console.ForegroundColor = ConsoleColor.DarkRed;
          break;
        case LogSeverity.Error:
          Console.ForegroundColor = ConsoleColor.Red;
          break;
        case LogSeverity.Warning:
          Console.ForegroundColor = ConsoleColor.Yellow;
          break;
        case LogSeverity.Info:
          Console.ForegroundColor = ConsoleColor.Cyan;
          break;
        case LogSeverity.Verbose:
          Console.ForegroundColor = ConsoleColor.Gray;
          break;
        case LogSeverity.Debug:
          Console.ForegroundColor = ConsoleColor.DarkGray;
          break;
        default:
          break;
      }
      Console.WriteLine(arg);
      Console.ResetColor();
      return Task.CompletedTask;
    }
  }
}
