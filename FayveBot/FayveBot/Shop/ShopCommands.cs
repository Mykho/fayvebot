﻿using System;
using System.Threading.Tasks;
using Discord.Addons.Interactive;
using Discord.Commands;

namespace FayveBot.Modules.Shop
{
  [Group("shop")]
  [Summary("Buy your items here")]
  public class ShopCommands : InteractiveBase
  {
    public readonly ShopService _service;

    public ShopCommands(ShopService service)
    {
      _service = service;
    }

    [Command]
    [Alias("open")]
    public async Task OpenShopCommandAsync()
    {
      await ReplyAsync(embed: await _service.GetShopEmbedAsync(Context.User));
    }

    [Command("buy")]
    public async Task BuyCommandAsync([Remainder] string itemName)
    {
      var reply = await _service.BuyItemReturnReplyAsync(Context.User.Id, itemName.ToLower());

      await ReplyAsync(reply);
    }
  }
}
