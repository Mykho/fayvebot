﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FayveBot.Utils.SQLite;

namespace FayveBot.Modules.Items
{
  public class ItemService
    {
    private readonly SQLiteService _sql;

    public ItemService(SQLiteService sql)
    {
      _sql = sql;

      var x = Task.Run(() => InitializeTableAsync());
    }

    public async Task<int> GetPriceAsync(string itemName)
    {
      var statement = $"select Price from Items where lower(Name) = \"{itemName.ToLower()}\"";
      var query = await _sql.QueryListAsync(statement, 1);

      return Convert.ToInt32(query.ElementAt(0));
    }

    private async Task InitializeTableAsync()
    {
      var statement = $@"CREATE TABLE if not exists ""Items"" (
	                      ""Id""	INTEGER NOT NULL DEFAULT 0 PRIMARY KEY AUTOINCREMENT UNIQUE,
	                      ""Name""	TEXT NOT NULL DEFAULT 'default',
	                      ""Emoji""	TEXT,
	                      ""Description""	TEXT DEFAULT 'default',
	                      ""Stats""	TEXT,
      	                ""Price""	INTEGER NOT NULL DEFAULT 0,
	                      ""InShop""	INTEGER);";

      await _sql.QuerySingleAsync(statement);
    }
  }
}
