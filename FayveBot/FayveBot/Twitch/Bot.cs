﻿using System;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Clients;
using TwitchLib.Communication.Models;

namespace FayveBot.Twitch
{
  public class TwitchBot
  {
    private TwitchClient _twitchClient;

    public TwitchBot() { }

    public void RunTwitchBot()
    {
      var credentials = new ConnectionCredentials("FayveBot", "kbppxx7uhaewzrsne21lpe8s5xa9f3");
      var clientOptions = new ClientOptions
      {
        MessagesAllowedInPeriod = 750,
        ThrottlingPeriod = TimeSpan.FromSeconds(30),
      };
      var customClient = new WebSocketClient(clientOptions);

      _twitchClient = new TwitchClient(customClient);

      _twitchClient.Initialize(credentials, "mykhotwitch");
      _twitchClient.Connect();
      _twitchClient.OnConnected += OnConnectedEventHandler;
    }

    private void OnConnectedEventHandler(object sender, OnConnectedArgs e)
    {
      _twitchClient.OnMessageReceived += MessageReceivedEventHandler;
      _twitchClient.OnChatCommandReceived += CommandReceivedHandler;
    }

    private void ConnectToFayve(object sender, OnConnectedArgs e)
    {
      _twitchClient.JoinChannel("fayveee");
    }

    private void MessageReceivedEventHandler(object sender, OnMessageReceivedArgs e)
    {
      switch(e.ChatMessage.Message)
      {
        case "LUL":
          _twitchClient.SendMessage(e.ChatMessage.Channel, "LUL");
          break;
        default:
          break;
      }
    }

    private void CommandReceivedHandler(object sender, OnChatCommandReceivedArgs e)
    {
      switch(e.Command.CommandText)
      {
        case "discord":
          _twitchClient.SendMessage(e.Command.ChatMessage.Channel, "https://discord.gg/DzWT8gs");
          break;
        case "instagram":
          _twitchClient.SendMessage(e.Command.ChatMessage.Channel, "https://www.instagram.com/fayvetwitch");
          break;
        default:
          break;
      }
    }

    private void ErrorLog(object sender, OnConnectionErrorArgs e)
    {
      Console.WriteLine(e.Error);
    }

    private void ClientLog(object sender, OnLogArgs e)
    {
      Console.WriteLine(e.Data);
    }

    private void OnJoinedChannelAsync(object sender, OnJoinedChannelArgs e)
    {
      Console.WriteLine($"Joined channel {e.Channel}");
    }
  }
}
