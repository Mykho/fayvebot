﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FayveBot.Utils.SQLite;

namespace FayveBot.Modules.Shop
{
  public class ShopController
  {
    private readonly SQLiteService _sql;

    public ShopController(SQLiteService sql)
    {
      _sql = sql;
    }

    public async Task<ShopClass> GetShopAsync()
    {
      var statement = $@"select Name, Description, Price from Items where Inshop = 1";
      var query = await _sql.QueryListAsync(statement, 3);
      var shop = new ShopClass();

      for(int i = 0; i < query.Count(); i = i + 3)
      {
        var name = query.ElementAt(i).ToString();
        var description = query.ElementAt(i + 1).ToString();
        var price = Convert.ToInt32(query.ElementAt(i + 2));

        shop.AddItem(name, description, price);
      }

      return shop;
    }

    public async Task BuyItemAsync(ulong userId, string itemName, int price)
    {
      var statement = $@" update users set gold = gold - {price} where id = {userId};
                          insert into ""Users.Inventory""(UserId, ItemId, amount)
                          select (select UserId
		                              from Users
		                              where id = {userId}), (select Id from Items where lower(name) = ""{itemName}""), 1";

      await _sql.QuerySingleAsync(statement);
    }
  }
}
