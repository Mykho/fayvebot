﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace FayveBot.Utils.Json
{
  public class JsonHandler
  {
    public T DeserializeString<T>(string textToDeserialize)
    {
      try
      {
        return JsonConvert.DeserializeObject<T>(textToDeserialize);
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
        return default(T);
      }
    }

    public void WriteJsonToSystemPath(object jsonObject, string systemPath)
    {
      try
      {
        using(StreamWriter streamWriter = new StreamWriter(systemPath, false, Encoding.UTF8))
        using(JsonTextWriter jsonTextWriter = new JsonTextWriter(streamWriter))
        {
          var settings = new JsonSerializerSettings() { Formatting = Formatting.Indented, };
          var jsonSerializer = JsonSerializer.Create(settings);

          jsonSerializer.Serialize(jsonTextWriter, jsonObject);
        }
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
      }
    }

    public T GetObjectJson<T>(string systemPath)
    {
      try
      {
        using(StreamReader streamReader = new StreamReader(systemPath, Encoding.UTF8))
        {
          string json = streamReader.ReadToEnd();
          T deserialized = JsonConvert.DeserializeObject<T>(json);

          if(deserialized != null)
          {
            return deserialized;
          }
          else
          {
            var newInstance = CreateNewInstance<T>();

            return newInstance;
          }
        }
      }
      catch(FileNotFoundException) // Json file was not found
      {
        Console.WriteLine($"Folder for {systemPath} was not found, it has been generated for you.");
        var newInstance = CreateNewInstance<T>();

        WriteJsonToSystemPath(newInstance, systemPath);
        return newInstance;
      }
      catch(DirectoryNotFoundException)
      {
        Console.WriteLine($"Generating folder at {systemPath}");
        CreateDirectory(systemPath);

        var returnJson = GetObjectJson<T>(systemPath);

        return returnJson;
      }
      catch(JsonSerializationException e)
      {
        Console.WriteLine(e);
        return default(T);
      }
      catch(Exception e) // Just in case anything unexpected happens
      {
        Console.WriteLine(e);
        return default(T);
      }
    }

    public string SerializeString(object textToSerialize)
    {
      try
      {
        return JsonConvert.SerializeObject(textToSerialize,
            Formatting.Indented, new JsonSerializerSettings()
            {
              ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
        return "";
      }
    }

    private T CreateNewInstance<T>()
    {
      try
      {
        var newInstance = (T)Activator.CreateInstance(typeof(T), new object[] { });

        return newInstance;
      }
      catch(MissingMethodException ex) // Type has no default constructor
      {
        Console.WriteLine($"{ex}\nType {typeof(T)} does not have a default constructor.");
        throw;
      }
    }

    private void CreateDirectory(string systemPath)
    {
      string systemPathWithoutFilename = systemPath.Remove(systemPath.LastIndexOf('/') + 1);

      try
      {
        Directory.CreateDirectory(systemPathWithoutFilename);
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
        throw;
      }
    }

    private object DeserializeString<T>(object p)
    {
      throw new NotImplementedException();
    }
  }
}