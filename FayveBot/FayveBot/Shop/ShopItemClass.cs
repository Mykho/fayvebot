﻿using System.Collections.Generic;
using System.Linq;

namespace FayveBot.Modules.Shop
{
  public class ShopClass
  {
    public IEnumerable<ShopItemClass> Items { get; set; } = new List<ShopItemClass>();

    public ShopClass() { }

    public void AddItem(string name = "", string description = "", int price = 0)
    {
      var list = Items.ToList();

      list.Add(new ShopItemClass(name, description, price));

      Items = list;
    }
  }

  public class ShopItemClass
  {
    public string Name { get; set; } = "";
    public string Description { get; set; } = "";
    public int Price { get; set; } = 0;

    public ShopItemClass() { }

    public ShopItemClass(string name = "", string description = "", int price = 0)
    {
      Name = name;
      Description = description;
      Price = price;
    }
  }
}
