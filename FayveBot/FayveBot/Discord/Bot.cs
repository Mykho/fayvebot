﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Addons.Interactive;
using Discord.Commands;
using Discord.WebSocket;
using FayveBot.Modules.Items;
using FayveBot.Modules.Shop;
using FayveBot.Utils.Currency;
using FayveBot.Utils.Json;
using FayveBot.Utils.LogClasses;
using FayveBot.Utils.Random;
using FayveBot.Utils.SQLite;
using Microsoft.Extensions.DependencyInjection;
using FayveBot.Modules.Inventory;

namespace FayveBot.Discord
{
  public class DiscordBot
  {
    private DiscordSocketClient _client;
    private CommandService _commands;
    private IServiceProvider _services;

    public DiscordBot() { }

    public async Task RunDiscordBotAsync()
    {
      var clientConfig = new DiscordSocketConfig
      {
        MessageCacheSize = 100,
        AlwaysDownloadUsers = true,
        LogLevel = LogSeverity.Info,
        DefaultRetryMode = RetryMode.RetryRatelimit
      };
      var commandConfig = new CommandServiceConfig()
      {
        LogLevel = LogSeverity.Info
      };
      _client = new DiscordSocketClient(clientConfig);
      _commands = new CommandService();

      _services = BuildServiceProvider();

      _client.MessageReceived += HandleCommandAsync;

      await InitializeCommandsAsync();
      await _client.StartAsync();
      await _client.LoginAsync(TokenType.Bot, "NjUzOTY1MjM0MjA1MjI5MDU5.Xe-qnQ.I8zTYYodfGUwBcgebqiursQp4Uk");
    }

    public IServiceProvider BuildServiceProvider() => new ServiceCollection()
      .AddSingleton(_client)
      .AddSingleton(_commands)
      .AddSingleton<JsonHandler>()
      .AddSingleton<RandomModule>()
      .AddSingleton<LogService>()
      .AddSingleton<SQLiteService>()
      .AddSingleton<CurrencyService>()
      .AddSingleton<ShopService>()
      .AddSingleton<ItemService>()
      .AddSingleton<InteractiveService>()
      .AddSingleton<InventoryService>()
      .BuildServiceProvider();
    
    private async Task InitializeCommandsAsync()
    {
      await _commands.AddModuleAsync<CurrencyCommands>(_services);
      await _commands.AddModuleAsync<DailyCommand>(_services);
      await _commands.AddModuleAsync<ShopCommands>(_services);
      await _commands.AddModuleAsync<ItemCommands>(_services);
      await _commands.AddModuleAsync<InventoryCommands>(_services);
    }

    private async Task HandleCommandAsync(SocketMessage messageReceived)
    {
      var message = messageReceived as SocketUserMessage;
      int argPos = 0;

      if(message is null)
        return;

      if(!message.HasCharPrefix('!', ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
        return;

      if(message.Author.IsBot)
        return;

      var context = new SocketCommandContext(_client, message);

      await _commands.ExecuteAsync(context, argPos, _services);
    }
  }
}
