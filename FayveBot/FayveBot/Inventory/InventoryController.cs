﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FayveBot.Utils.SQLite;

namespace FayveBot.Modules.Inventory
{
  public class InventoryController
  {
    private readonly SQLiteService _sql;

    public InventoryController(SQLiteService sql)
    {
      _sql = sql;
    }

    public async Task<UserInventory> GetInventoryAsync(ulong id)
    {
      var statement = $@" {InsertIntoUsersQuery(id)}
                          select ""Users.Inventory"".Amount, Items.Name, Items.Description
                          from ""Users.Inventory""
                          inner join Items on ""Users.Inventory"".ItemId=Items.Id
                          where userId = (select UserId
				                                  from Users
				                                  where id = {id});";
      var query = await _sql.QueryListAsync(statement, 3);
      var inventory = new UserInventory();

      if(query is null)
        return inventory;

      for(int i = 0; i < query.Count(); i = i + 3)
      {
        var amount = Convert.ToInt32(query.ElementAt(i));
        var name = query.ElementAt(i + 1).ToString();
        var description = query.ElementAt(i + 2).ToString();

        inventory.AddItem(name, description, amount);
      }

      return inventory;
    }

    private string InsertIntoUsersQuery(ulong userId)
    {
      return $@" insert into Users(id)
                          select ({userId})
                          where not exists (select *
					                                  from Users
					                                  where id = {userId});";
    }
  }
}