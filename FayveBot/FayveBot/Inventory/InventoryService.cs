﻿using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using FayveBot.Utils.LogClasses;
using FayveBot.Utils.SQLite;

namespace FayveBot.Modules.Inventory
{
  public class InventoryService
  {
    private readonly SQLiteService _sql;
    private readonly LogService _logger;
    private readonly InventoryController _controller;

    public InventoryService(SQLiteService sql, LogService logger)
    {
      _sql = sql;
      _logger = logger;
      _controller = new InventoryController(_sql);

      var init = Task.Run(() => InitialiseTableAsync());
    }

    public async Task<Embed> GetInventoryEmbedAsync(SocketUser user)
    {
      var inventory = await _controller.GetInventoryAsync(user.Id);
      var embed = new EmbedBuilder()
      {
        Title = $"{user}'s inventory",
        Description = "You seem to have infinite cookies, where do those come from?",
        Color = new Color(16741072),
        ThumbnailUrl = user.GetAvatarUrl()
      };

      foreach(var item in inventory.Items)
      {
        embed.AddField(new EmbedFieldBuilder()
        {
          Name = item.Name,
          Value = item.Description
        });
      }

      return embed.Build();
    }

    private async Task InitialiseTableAsync()
    {
      await _sql.QuerySingleAsync(@"CREATE TABLE if not exists ""Users.Inventory"" (
	                                ""UserId""	INTEGER NOT NULL DEFAULT 0,
	                                ""ItemId""	INTEGER NOT NULL DEFAULT 0,
	                                ""Amount""	INTEGER NOT NULL DEFAULT 0);");
    }

    public async Task<string> GetInventoryAsync(SocketUser user)
    {
      var inventory = await _controller.GetInventoryAsync(user.Id);
      var text = "";

      foreach(var item in inventory.Items)
      {
        text += $"\r\n{item.Name}\t{item.Description}\t{item.Amount}x\r\n";
      }

      return text;
    }
  }
}
