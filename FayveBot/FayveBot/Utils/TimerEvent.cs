﻿using System;
using System.Threading.Tasks;
using Discord;
using FayveBot.Utils.LogClasses;

namespace FayveBot.Utils
{
  public class DailyTimer
  {
    public delegate Task EventTimer();
    public event EventTimer OnTimerAsync;

    private readonly LogService _logger;

    private DateTime _resetTime;
    private TimeSpan _timeToRaiseEvent { get; set; }

    public DailyTimer(LogService logger, DateTime resetTime)
    {
      _logger = logger;
      _resetTime = resetTime;

      _timeToRaiseEvent = GetTimeUntilNextReset();
    }

    public async Task RunTimerAsync()
    {
      await _logger.ConsoleWrite(LogSeverity.Debug, this, $"{_timeToRaiseEvent.ToString()} until next timer");
      await Task.Delay(_timeToRaiseEvent);
      await OnTimerAsync();

      _timeToRaiseEvent = new TimeSpan(hours: 24, minutes: 0, seconds: 0);
    }

    public TimeSpan GetTimeUntilNextReset()
    {
      var now = DateTime.Now;

      if(DateTime.Compare(_resetTime, now) >= 0)
      {
        return _resetTime.Subtract(now);
      }
      else
      {
        return _resetTime.AddDays(1).Subtract(now);
      }
    }
  }
}
