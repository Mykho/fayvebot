﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using FayveBot.Utils.Random;
using FayveBot.Utils.Json;
using FayveBot.Utils.LogClasses;
using FayveBot.Utils.SQLite;

namespace FayveBot.Utils.Currency
{
  public class CurrencyService
  {
    private readonly DiscordSocketClient _client;
    private readonly CurrencyController _controller;
    private readonly RandomModule _random;
    private readonly LogService _logger;
    private readonly SQLiteService _sql;
    private readonly DailyTimer _timer;

    private int _findGoldChance { get; } = 1;

    public int DailyAmount { get; } = 10;

    public CurrencyService(JsonHandler json, DiscordSocketClient client, RandomModule random, LogService logger, SQLiteService sql)
    {
      _client = client;
      _sql = sql;
      _random = random;
      _logger = logger;
      _controller = new CurrencyController(_sql, _logger);
      _timer = new DailyTimer(_logger, DateTime.Now.Date.AddHours(7).AddMinutes(0).AddSeconds(0));
      _client.MessageReceived += MessageReceivedEventHandler;
      _timer.OnTimerAsync += new DailyTimer.EventTimer(TimerElapsedEventHandlerAsync);



      var x = Task.Run(() => _timer.RunTimerAsync());

      _logger.ConsoleWrite(new LogMessage(LogSeverity.Info, "Service", "Currency Service initialised"));
    }

    private async Task TimerElapsedEventHandlerAsync()
    {
      var statement = $@"update users set dailyclaim = 0";

      await _sql.QuerySingleAsync(statement);

      var x = Task.Run(() => _timer.RunTimerAsync());
    }

    public TimeSpan GetTimeUntilNextReset() => _timer.GetTimeUntilNextReset();

    // True if accepted
    // False if failed
    public async Task<bool> ClaimDailyAsync(ulong id)
    {
      return await _controller.AddDailyIfNotClaimedAsync(id, DailyAmount);
    }

    public async Task<CurrencyUser> GetUserAsync(ulong id) => await _controller.GetUser(id);

    private async Task MessageReceivedEventHandler(SocketMessage arg)
    {
      if(arg.Author.IsBot || _random.Chance100() > _findGoldChance)
        return;

      await _controller.AddCurrency(arg.Author.Id);
      var message = SendCurrencyMessageAndDeleteAsync(arg.Channel as SocketTextChannel, arg.Author as SocketGuildUser);
    }

    private async Task SendCurrencyMessageAndDeleteAsync(SocketTextChannel channel, SocketGuildUser user)
    {
      var message = await channel.SendMessageAsync($"**{user}** found a gold coin <:coin:561105631940050946>!");
      await Task.Delay(3000);
      await message.DeleteAsync();
    }
  }
}
