﻿using System.Threading.Tasks;
using FayveBot.Discord;
using FayveBot.Twitch;

namespace FayveBot
{
  public class Bot
  {
    private TwitchBot _twitchBot;
    private DiscordBot _discordBot;

    static void Main(string[] args) => new Bot()
      .RunBotAsync()
      .GetAwaiter()
      .GetResult();

    public async Task RunBotAsync()
    {
      _twitchBot = new TwitchBot();
      _discordBot = new DiscordBot();

      _twitchBot.RunTwitchBot();
      await _discordBot.RunDiscordBotAsync();

      await Task.Delay(-1);
    }
  }
}
