﻿using System.Threading.Tasks;
using Discord.Commands;

namespace FayveBot.Utils.Currency
{
  [Group("daily")]
  public class DailyCommand : ModuleBase<SocketCommandContext>
  {
    private readonly CurrencyService _service;

    public DailyCommand(CurrencyService service)
    {
      _service = service;
    }

    [Command]
    public async Task DailyCommandAsync()
    {
      var claimed = await _service.ClaimDailyAsync(Context.User.Id);
      var okMessage = $"You just got {_service.DailyAmount} coins!";
      var notOkMessage = $"Your daily bonus is not ready yet, **{Context.User}**";

      if(!claimed)
      {
        await ReplyAsync(okMessage);
      }
      else
      {
        var timeUntilReset = _service.GetTimeUntilNextReset();
        await ReplyAsync($"{notOkMessage}\r\n" +
          $"Time until you can claim the next daily bonus:\t" +
          $"**{timeUntilReset.Hours} hours, {timeUntilReset.Minutes} minutes and {timeUntilReset.Seconds} seconds**");
      }
    }
  }
}
