﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace FayveBot.Utils.Random
{
  public class RandomModule
  {
    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

    public int IntegerRange(int min, int max)
    {
      if(max < min)
        return min;

      var randomByte = new byte[4];
      rng.GetBytes(randomByte);
      var randomInteger = BitConverter.ToUInt32(randomByte, 0);
      System.Random random = new System.Random((int)randomInteger);
      return (random.Next(min, max));
    }

    public int PositiveInteger(int max)
    {
      return IntegerRange(0, max);
    }

    public int Chance100()
    {
      return IntegerRange(0, 100);
    }

    public int Index(dynamic list)
    {
      try
      {
        return PositiveInteger(list.Count);
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
        return 0;
      }
    }

    public T FromList<T>(IReadOnlyCollection<T> source)
    {
      try
      {
        var list = source.ToList();

        if(list.Count == 0)
          return default(T);

        var index = Index(list);

        return list.ElementAt(index);
      }
      catch(Exception e)
      {
        Console.WriteLine(e);
        return source.ElementAt(0);
      }
    }
  }
}