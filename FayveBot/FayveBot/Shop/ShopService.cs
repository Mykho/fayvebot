﻿using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using FayveBot.Modules.Items;
using FayveBot.Utils.Currency;
using FayveBot.Utils.SQLite;

namespace FayveBot.Modules.Shop
{
  public class ShopService
  {
    private readonly ShopController _controller;
    private readonly SQLiteService _sql;
    private readonly CurrencyService _currency;
    private readonly ItemService _items;

    public ShopService(SQLiteService sql, CurrencyService currency, ItemService items)
    {
      _sql = sql;
      _currency = currency;
      _items = items;
      _controller = new ShopController(_sql);
    }

    public async Task<Embed> GetShopEmbedAsync(SocketUser user)
    {
      var shop = await _controller.GetShopAsync();
      var embed = new EmbedBuilder()
      {
        Title = $"It's the Sweet Shop!",
        Description = "Buy any item you want with command `shop buy <item name>`",
        Color = new Color(16741072),
        ThumbnailUrl = user.GetAvatarUrl()
      };

      foreach(var item in shop.Items)
      {
        embed.AddField(new EmbedFieldBuilder()
        {
          IsInline = false,
          Name = $"{item.Price} Gold - {item.Name}",
          Value = $"{item.Description}"
        });
      }

      return embed.Build();
    }
     
    public async Task<string> BuyItemReturnReplyAsync(ulong userId, string itemName)
    {
      var user = await _currency.GetUserAsync(userId);
      var price = await _items.GetPriceAsync(itemName);

      if(price > user.Currency)
      {
        return "You do not have enough Gold to buy this item";
      }
      else
      {
        await _controller.BuyItemAsync(userId, itemName, price);

        return "You just got an item!";
      }
    }
  }
}
