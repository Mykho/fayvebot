﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using FayveBot.Utils.Currency;
using FayveBot.Utils.LogClasses;
using FayveBot.Utils.SQLite;

namespace FayveBot.Utils.Currency
{
  public class CurrencyController
  {
    private readonly SQLiteService _sql;
    private readonly LogService _logger;

    private string _systempath { get; set; }

    public CurrencyController(SQLiteService sql, LogService logger)
    {
      _sql = sql;
      _logger = logger;
    }

    public async Task<CurrencyUser> GetUser(ulong userId)
    {
      var userData = await _sql.QueryListAsync($@"insert into users(id)
                                                  select {userId} where not exists(select * from users where id = {userId});
                                                  select id, gold from users where id = {userId}", 2);
      var profile = new CurrencyUser
      {
        Id = Convert.ToUInt64(userData.ElementAt(0)),
        Currency = Convert.ToInt32(userData.ElementAt(1))
      };

      return profile;
    }

    public async Task AddCurrency(ulong userId)
    {
      var statement = $@" insert into users(id) select {userId} where not exists(select * from users where id = {userId});
                            update users set gold = gold + 1 where id = {userId}";
      await _sql.QuerySingleAsync(statement);

      await _logger.ConsoleWrite(LogSeverity.Debug, this, $"Currency added to {userId}");
    }

    // Returns true if ok, otherwise false
    public async Task<bool> AddDailyIfNotClaimedAsync(ulong id, int dailyAmount)
    {
      var statement = $@" insert into users(id)
                          select ({id}) 
                          where not exists (select * 
                  					                from users
					                                  where id = {id});
                          select dailyclaim from users where id = {id};";
      var statement2 = $@"update users set gold = gold + {dailyAmount} where id = {id} and dailyclaim = 0;
                          update users set dailyclaim = 1 where id = {id};";
      var query = await _sql.QueryListAsync(statement, 1);

      await _sql.QuerySingleAsync(statement2);
      
      return Convert.ToBoolean(query.ElementAt(0));
    }
  }
}
