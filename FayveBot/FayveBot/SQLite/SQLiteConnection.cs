﻿using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using System.Threading.Tasks;
using System.Linq;

namespace FayveBot.Utils.SQLite
{
  public class SQLiteService
  {
    private string _connectionString { get; set; }

    public SQLiteService(string filename = null)
    {
      _connectionString = $"Filename={filename ?? "database.db"}";
    }

    public async Task<IEnumerable<object>> GetList(string command, int rows)
    {
      if(command == "")
        return null;

      var list = new List<object>();

      using(var connection = new SqliteConnection(_connectionString))
      using(var sqliteCommand = new SqliteCommand(command, connection))
      {
        await connection.OpenAsync();
        sqliteCommand.Prepare();

        using(var query = await sqliteCommand.ExecuteReaderAsync())
        {
          while(await query.ReadAsync())
          {
            var storage = new object[rows];

            query.GetValues(storage);
            list.Add(storage);
          }
        }
      }

      return list;

    }

    public async Task<List<object>> QueryListAsync(string command, int rows)
    {
      if(command == "")
        return null;

      using(var connection = new SqliteConnection(_connectionString))
      using(var sqliteCommand = new SqliteCommand(command, connection))
      {
        await connection.OpenAsync();
        sqliteCommand.Prepare();

        using(var query = await sqliteCommand.ExecuteReaderAsync())
        {
          var objectsList = new List<object>();

          while(await query.ReadAsync())
          {
            for(int i = 0; i < rows; i++)
            {
              objectsList.Add(query[i]);
            }
          }

          return objectsList;
        }
      }
    }

    public async Task<List<object>> QuerySingleAsync(string command, params string[] objects)
    {
      if(command == "")
        return null;

      using(var connection = new SqliteConnection(_connectionString))
      using(var sqliteCommand = new SqliteCommand(command, connection))
      {
        await connection.OpenAsync();
        sqliteCommand.Prepare();

        try
        {
          using(var query = await sqliteCommand.ExecuteReaderAsync())
          {
            var queriedItems = new List<object>();

            for(int i = 0; i < objects.Count(); i++)
            {
              var askFor = objects.ElementAt(i);
              var value = query[askFor];

              queriedItems.Add(value);
            }

            return queriedItems;
          }
        }
        catch(Exception e)
        {
          connection.Close();
          sqliteCommand.Cancel();
          Console.WriteLine(e);
          throw;
        }
      }
    }
  }
}