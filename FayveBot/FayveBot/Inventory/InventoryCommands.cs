﻿using System;
using System.Threading.Tasks;
using Discord.Addons.Interactive;
using Discord.Commands;

namespace FayveBot.Modules.Inventory
{
  [Group("inventory")]
  [Alias("inv", "i")]
  public class InventoryCommands : ModuleBase<SocketCommandContext>
  {
    private readonly InventoryService _service;

    public InventoryCommands(InventoryService service)
    {
      _service = service;
    }

    [Command]
    public async Task OpenInventoryCommandAsync()
    {
      await ReplyAsync(embed: await _service.GetInventoryEmbedAsync(Context.User));
    }
  }
}
