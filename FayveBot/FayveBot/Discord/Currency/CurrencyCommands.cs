﻿using Discord.Commands;

namespace FayveBot.Utils.Currency
{
  [Summary("Group of commands for the currency system")]
  public class CurrencyCommands : ModuleBase<SocketCommandContext>
  {
    private readonly CurrencyService _currency;

    public CurrencyCommands(CurrencyService currency)
    {
      _currency = currency;
    }
  }
}
