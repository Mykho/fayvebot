﻿using System.Collections.Generic;
using System.Linq;

namespace FayveBot.Modules.Inventory
{
  public class UserInventory
  {
    public IEnumerable<Item> Items { get; set; } = new List<Item>();

    public void AddItem(string name = "default", string description = "default", int amount = 0)
    {
      var items = Items.ToList();
      var newItem = new Item();

      newItem.AddItem(name, description, amount);
      items.Add(newItem);

      Items = items;
    }

    public class Item
    {
      public string Name { get; private set; }
      public string Description { get; private set; }
      public int Amount { get; private set; }

      public void AddItem(string name = "default", string description = "default", int amount = 0)
      {
        Name = name;
        Description = description;
        Amount = amount;
      }
    }
  }
}
