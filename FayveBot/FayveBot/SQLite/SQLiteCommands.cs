﻿using System;
using System.Threading.Tasks;
using Discord.Addons.Interactive;
using Discord.Commands;

namespace FayveBot.Utils.SQLite
{
  [Group("sql")]
  [RequireOwner]
  public class SQLiteCommands : InteractiveBase
  {
    private readonly SQLiteService _service;

    public SQLiteCommands(SQLiteService service)
    {
      _service = service;
    }

    [Command(RunMode = RunMode.Async)]
    [Summary("Execute a single SQL query")]
    public async Task ExecuteAsync([Remainder] string query)
    {
      var timeout = new TimeSpan(0, 0, seconds: 30);

      await ReplyAndDeleteAsync($"Execute query? `yes/no`\r\n{query}", timeout: timeout);
      var response = await NextMessageAsync(timeout: timeout);

      if(response is null)
        return;

      if(response.Content.Contains("yes"))
      {
        await ReplyAndDeleteAsync("Done", timeout: timeout);
        await _service.QuerySingleAsync(query);
      }
    }
  }
}